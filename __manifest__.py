# -*- coding: utf-8 -*-
{
    'name': "Customer Credit Limit",
    'summary': """
        """,
    'description': """
        
    """,
    'author': "Icreative Technolabs",
    'website': "http://www.icreativetechnolabs.com",
    'category': 'Sales',
    'version': '0.1',
    'depends': ['sale'],
    'data': [
        'views/res_partner_view.xml',
        'wizard/wizard_credit_limit.xml',
    ],
}