Added New Fields in res.partner
-------------------------------

is_credit_limit enable fields credit_limit and credit_policy
credit_limit field set partner's maximun credit 
credit_policy contains two types are check on so and check on invoice


Set partner's credit Limit
---------------------------

Only Sales Manager have access rights to set credit limit on res.partner
To set credit limit goto Set Credit Limit action window which is on res.partner form view


Credit Policy
-------------

1) Check credit while confirming SO: 
	Check credit limit when confirm SALE ORDER of specific customer.

2) Check credit while validating invoices:
	Check credit limit when validate INVOICE of specific customer.
