# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError


class SaleOrder(models.Model):
    _inherit = "sale.order"

    @api.multi
    def action_confirm(self):
        if self.partner_id.credit_policy == 'confirm_so' and self.partner_id.is_credit_limit:
            if not self.amount_total <= self.partner_id.credit_limit:
                raise UserError(_("Call Manager to increase partner's credit limit to confirm this sale order."))
        return super(SaleOrder, self).action_confirm()
