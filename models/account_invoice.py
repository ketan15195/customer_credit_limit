# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError


class AccountInvoice(models.Model):
    _inherit = "account.invoice"

    @api.multi
    def action_invoice_open(self):
        if self.partner_id.credit_policy == 'validate_invoice' and self.partner_id.is_credit_limit:
            if not self.amount_total <= self.partner_id.credit_limit:
                raise UserError(_("Call Manager to increase partner's credit limit to validate this Invoice."))
        return super(AccountInvoice, self).action_invoice_open()