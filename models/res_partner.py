# -*- coding: utf-8 -*-

from odoo import fields, models


class ResPartner(models.Model):
    _inherit = 'res.partner'

    is_credit_limit = fields.Boolean(string="Is Credit Limit", default=False)
    credit_limit = fields.Float(string="Credit Limit")
    credit_policy = fields.Selection(
        [('confirm_so', 'Check credit while confirming SO'), 
        ('validate_invoice', 'Check credit while Validating Inoices')], string="Credit Policy")
