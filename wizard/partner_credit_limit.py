# -*- coding: utf-8 -*-

from odoo import api, fields, models


class CreditLimit(models.TransientModel):
    _name = "partner.credit.limit"

    partner_id = fields.Many2one('res.partner')
    credit_limit = fields.Float(string="Credit Limit")
    credit_policy = fields.Selection(
        [('confirm_so', 'Check credit while confirming SO'),
         ('validate_invoice', 'Check credit while Validating Inoices')], string="Credit Policy")

    @api.multi
    def update_credit(self):
        for wizard in self:
            wizard.partner_id.write(
                {'is_credit_limit': True, 'credit_limit': wizard.credit_limit, 'credit_policy': wizard.credit_policy})
        return True
